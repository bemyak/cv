// Author: Sergei Gureev <opensource@bemyak.net>
// Source: https://gitlab.com/bemyak/cv
// License: CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
//          (or contact me)

#let cv_header(name: "", position: "") = {
	grid(
		columns: (1fr, 1fr),
		align(left,
			text(fill: blue, size: 20pt, weight: 600, name)),
		align(right,
			text(fill: blue, size: 20pt, position)),
	)

	line(
		length: 100%,
		stroke: (thickness:1.5pt, cap: "round")
	)
}


#let contacts(color: black, contacts: []) = {
	let mid = calc.ceil(contacts.len()/2)
	stack(
		dir: ltr,
		grid(
			column-gutter: 1.5em,
			row-gutter: 0.5em,
			columns: 3,
			..contacts.slice(0, mid).map(contact => {
				(
					align(center, text(fill: color, weight: "bold", contact.icon)),
					align(left, text(fill: color, weight: "bold", contact.name)),
					align(left, link(contact.link, contact.text)),
				)
			}).flatten(),
		),
		align(right,
			grid(
				column-gutter: 1.5em,
				row-gutter: 0.5em,
				columns: 3,
				..contacts.slice(mid, contacts.len()).map(contact => {
					(
						align(center, text(fill: color, weight: "bold", contact.icon)),
						align(left, text(fill: color, weight: "bold", contact.name)),
						align(right, link(contact.link, contact.text)),
					)
				}).flatten(),
			)
		)
	)
}


#let job(start: none, end: none, position: "", team: none, company: "", website: none, tech: none, content) = {
	stack(
		dir: ltr,
		spacing: 1em,
		box(width: 2cm, text(fill: blue, weight: "bold")[
			#if start != none [ #start -- #linebreak() ]
			#if end != none [ #end ]
		]),
		box(width:100%-25mm)[
			#if website == none [
				#company
			] else [
				#link(website, company)
			]
			#linebreak()
			#emph()[
				#position
				#if team != none [ \@ #team ]
			]

			#show list: it => {
				set list(indent: 1em)
				block(above: 1.5em, it)
			}
			#content

			#if tech != none [ *Technologies: * #tech. ]
		],
	)
}

#let study(start: none, end: none, what: "", where: "", website: none) = {
	stack(
		dir: ltr,
		spacing: 1em,
		box(width: 2cm, text(fill: blue, weight: "bold")[
			#if start != none [ #start]#if end != none [-#end]
		]),
		box(width:100%-25mm)[
			#what
			#linebreak()
			#emph()[
				#if website == none [
					#where
				] else [
					#link(website, where)
				]
			]
		]
	)
}

#let hobbies(hobbies) = {
	grid(
		columns: 2,
		column-gutter: 1.5em,
		row-gutter: 0.8em,
		..hobbies.map(hobby => {
			(
				align(center, text(fill: blue, weight: "bold", hobby.icon)),
				[#hobby.text]
			)
		}).flatten(),
	)
}
