// Author: Sergei Gureev <opensource@bemyak.net>
// Source: https://gitlab.com/bemyak/cv
// License: CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
//          (or contact me)

#import "@preview/fontawesome:0.1.0": *
#import "repr.typ": cv_header, contacts, job, study, hobbies

/////////////////////////////// GENERAL SETTINGS ///////////////////////////////

#let blue = rgb(0, 108, 183)
#let default_size = 10pt;

#set page(paper: "a4")

#set document(
	title: "CV — Software Developer",
	author: "Sergei Gureev",
	date: auto,
)

#show heading: it => {
	set block(above: 2em, below: 1.5em)
	text(fill: blue, size: default_size * 1.2, it)
}

#set par(justify: true, leading: 0.78em, linebreaks: "optimized")
#show par: set block(below: 1.5em)

#set text(
	font: "AlgolRevived",
	size: default_size,
	hyphenate: false,
)

#set underline(offset: 2.6pt)


/////////////////////////////////// CONTENT ////////////////////////////////////

#cv_header(name: "Sergei Gureev", position: "Software Developer")

#contacts(color: blue, contacts: (
	(
		icon: fa-envelope(fa-set: "Free Solid"),
		name: "Email",
		link: "mailto:job@bemyak.net",
		text: "job@bemyak.net",
	),
	(
		icon: fa-linkedin(fa-set: "Brands"),
		name: "LinkedIn",
		link: "https://www.linkedin.com/in/bemyak/",
		text: "linkedin.com/in/bemyak",
	),
	(
		icon: fa-house(fa-set: "Free Solid"),
		name: "Blog",
		link: "https://blog.bemyak.net",
		text: "blog.bemyak.net",
	),
	(
		icon: fa-github(fa-set: "Brands"),
		name: "GitHub",
		link: "https://github.com/bemyak",
		text: "github.com/bemyak",
	),
	(
		icon: fa-gitlab(fa-set: "Brands"),
		name: "GitLab",
		link: "https://gitlab.com/bemyak",
		text: "gitlab.com/bemyak",
	),
	(
		icon: fa-mastodon(fa-set: "Brands"),
		name: "Mastodon",
		link: "https://lor.sh/@bemyak",
		text: "@bemyak@lor.sh",
	),
))


= Personal Profile

I've always been curious about how this computer thing works, so over the last two decades I was going down the rabbit hole of Computer Science and ended up touching almost every aspect of it.
Although my main focus is #underline[Backend Development], I also have experience in many other areas: Frontend, System Administration, Game Development, Embedded Development, and ML.
I'm driven by the desire to get to the root of things, but sometimes I let myself get carried away into a completely new area.

Having international experience, I've worked with people of diverse levels and backgrounds.
For a short time, I was a #underline[Team Leader] but decided to return to the #underline[Individual Contributor] track because that's where my passion is.

My rich technical toolbelt and solid soft skills allow me to approach challenges more efficiently.
I believe that finding the right solution isn't always about choosing the technology, but often it's about clarifying the requirements in order to deliver the most value upfront.


= Employment History

#stack(
spacing: 1.5em,
job(
	start: "Nov 2022",
	end: "Present",
	company: "Sharper Shape Oy",
	website: "https://sharpershape.com/",
	team: "Instrumentation Team",
	position: "Senior Software Developer",
	tech: "Go, Python, ZeroMQ, Protobuf, LiDAR, GNSSIMU, Ansible, Rust",
)[
I transferred to a Hardware Team to gain experience with #underline[physical devices].
My role is to develop software to control a sensor system that collects imagery and LiDAR data from a helicopter.

A key aspect of the role is the #underline[reliability] of the code: a single bug can halt the data collection, resulting in significant financial loss.
],

job(
	start: "May 2022",
	end: "Nov 2022",
	company: "Sharper Shape Oy",
	website: "https://sharpershape.com/",
	team: "Backend Team",
	position: "Senior Software Developer",
	tech: "Go, Postgres, SQL, AWS, Kubernetes, Prometheus",
)[
I was excited to join a smaller start-up, where my work could have a greater impact. In this role I:
- Developed backend services based on Postgres and PostGIS
- Upstreamed changes to the open-source Tegola vector tile server
- Integrated a Prometheus-based monitoring system
],
)

#pagebreak()

#stack(
spacing: 1.5em,
job(
	start: "Aug 2019",
	end: "Apr 2022",
	company: "RELEX Solutions",
	website: "https://www.relexsolutions.com/",
	team: "Monitoring&Logging Team",
	position: [Software Developer #sym.arrow.r.double Senior Software Developer],
	tech: "Go, Rust, Prometheus, Grafana, AWS, Azure, Kubernetes, Terraform",
)[
I joined the team to help build a #underline[monitoring platform] that collects metrics and logs. The solution had to be highly available and performant, with peak log throughput reaching 1Tb/s. I arranged #underline[training] for developers to spread best practices for exposing metrics and logs from their applications.

For half a year I took on the role of #underline[Team Leader]. During this time, I organised peer reviews and held personal development and promotion discussions.
],

job(
	start: "Sep 2015",
	end: "May 2019",
	company: "LANIT",
	website: "https://www.lanit.ru/en/",
	position: "Software Developer",
	tech: "Java, Kotlin, Elm, JavaScript, Angular, OracleDB, Cassandra, Elasticsearh, Logstash, Kibana, Ansible, Docker, Kubernetes",
)[
Our small team provided outsourcing services to the largest mobile operators. I worked on several critical applications, including:
- Anti-spam: A service that processes every call in an operator's network to block or redirect it based on user-defined rules and internal heuristics
- Stock market data processing and analysis pipeline
],

job(
	start: "Apr 2011",
	end: "Sep 2015",
	company: "LANIT - BPM",
	website: "https://www.lanit.ru/en/about/departments/16732/",
	position: "System Administrator",
	tech: "Linux, ESXi, CI/CD, OracleDB, Weblogic, Atlassian products",
)[
I started my career as a System Administrator, supporting a department of 30 people that quickly grew to 250.
]
)


= Education & Courses

#study(start: 2016, what: "Summer internship, Information Security & Pentesting", where: "Positive Technologies")

#study(start: 2011, end: 2013, what: "MTech, Information Technology", where: "Moscow State Technological University \"Stankin\"")

#study(start: 2007, end: 2011, what: "BTech, Information Technology", where: "Moscow State Technological University \"Stankin\"")


= Doing in free time

#hobbies((
	(
		icon: fa-user-friends(fa-set: "Free Solid"),
		text: "Spending time with my family",
	),
	(
		icon: fa-rust(fa-set: "Brands"),
		text: "Programming for fun, mainly in Rust",
	),
	(
		icon: fa-book(fa-set: "Free Solid"),
		text: "Learning Finnish (somewhere around B1)",
	),
	(
		icon: fa-biking(fa-set: "Free Solid"),
		text: "Biking & Bouldering (beat 6c max)",
	),
	(
		icon: fa-gamepad(fa-set: "Free Solid"),
		text: "Playing PC Games (love Tunic, Outer Wilds, and Souls-like)",
	),
		(
		icon: fa-dice-d20(fa-set: "Free Solid"),
		text: "Playing Pen and Paper RPGs and tabletop games (D&D 5e, Pathfinder 2e, Root)",
	),
))
