// Author: Sergei Gureev <opensource@bemyak.net>
// Source: https://gitlab.com/bemyak/cv
// License: CC BY-NC-SA 4.0 https://creativecommons.org/licenses/by-nc-sa/4.0/
//          (or contact me)

#import "@preview/fontawesome:0.1.0": *
#import "repr.typ": cv_header, contacts

/////////////////////////////// GENERAL SETTINGS ///////////////////////////////

#let blue = rgb(0, 108, 183)
#let default_size = 10pt;

#set page(paper: "a4")

#set document(
	title: "CV — Software Developer",
	author: "Sergei Gureev",
	date: auto,
)

#show heading: it => {
	set block(above: 2em, below: 1em)
	align(center, text(fill: blue, size: 20pt, it))
}

#set par(justify: true, leading: 0.78em, linebreaks: "optimized")
#show par: set block(below: 1.5em)

#set text(
	font: "AlgolRevived",
	size: default_size,
	hyphenate: false,
)

#set underline(offset: 2.6pt)


/////////////////////////////////// CONTENT ////////////////////////////////////

#cv_header(name: "Sergei Gureev", position: "Software Developer")

#contacts(color: blue, contacts: (
	(
		icon: fa-envelope(fa-set: "Free Solid"),
		name: "Email",
		link: "mailto:job@bemyak.net",
		text: "job@bemyak.net",
	),
	(
		icon: fa-linkedin(fa-set: "Brands"),
		name: "LinkedIn",
		link: "https://www.linkedin.com/in/bemyak/",
		text: "linkedin.com/in/bemyak",
	),
	(
		icon: fa-house(fa-set: "Free Solid"),
		name: "Blog",
		link: "https://blog.bemyak.net",
		text: "blog.bemyak.net",
	),
	(
		icon: fa-github(fa-set: "Brands"),
		name: "GitHub",
		link: "https://github.com/bemyak",
		text: "github.com/bemyak",
	),
	(
		icon: fa-gitlab(fa-set: "Brands"),
		name: "GitLab",
		link: "https://gitlab.com/bemyak",
		text: "gitlab.com/bemyak",
	),
	(
		icon: fa-mastodon(fa-set: "Brands"),
		name: "Mastodon",
		link: "https://lor.sh/@bemyak",
		text: "@bemyak@lor.sh",
	),
))


= Cover Letter

#set text(size: 12pt);


Dear Hiring Manager,

I am writing to express my interest in the Backend Developer position at DataCrunch. I am a software developer with a strong background in backend development and a passion for building scalable and reliable systems. I believe my skills and experience make me a good fit for this role. I've been using Go extensively for the last 5 years, and the current project I'm working on is written in Python. Please refer to the attached CV for more details.

I'm interested in working at DataCrunch because your product is built on a tight fusion between software and hardware, and it's exactly the area I'm intrigued with. Providing services for Machine learning is an extremely hot topic, and I believe in the company's ultimate success.

Thank you for considering my application.

-- \
Best regards,\
Sergei Gureev
